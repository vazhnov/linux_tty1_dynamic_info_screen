# Dynamic info screen for Linux tty1 console

Goal: instead of login, put maximum useful information to one TTY screen and update it continuously.

Why: sometime I want to see IP-addresses of my virtual machines and hardware servers without login to it.

## How to run any script on tty1

### Systemd

This recipe is based on article [Htop on getty](https://habr.com/post/304594/) (ru).

Create file `/etc/systemd/system/infoscreen@.service`:

```ini
.include /lib/systemd/system/getty@.service

[Unit]
Description=Dynamic info screen on %I
Documentation=https://gitlab.com/vazhnov/linux_tty0_dynamic_info_screen

[Service]
User=nobody
Group=nogroup
Environment=
Environment=TERM=linux
ExecStart=
ExecStart=/usr/bin/watch -n 3 --color --no-title "date; hostname; cat /etc/issue; ip -color -oneline addr show scope global | column -t; echo; lsblk; echo; df -h | grep -v ^tmpfs"
StandardInput=tty-fail
StandardOutput=tty
```

Enable:

```bash
sudo systemctl daemon-reload
sudo systemctl disable getty@tty1.service
sudo systemctl enable infoscreen@tty1.service
```

### sysv-rc

To be done.

### upstart

To be done.

## What to run on tty1

* Some custom shell-script
* htop
* screenfetch
* neofetch
* ttyload
* tty-clock
* glances
* inxi

Here is more information:

### Custom shell script

Example:

```bash
watch -n 3 --color --no-title "date; hostname; cat /etc/issue; ip -color -oneline addr show scope global | column -t; echo; lsblk; echo; df -h | grep -v ^tmpfs"
```

* Pros: no need to install anything to operation system.
* Cons: all information may not fit to screen resolution.

## Known issues

To be done.

## Links

* [This project](https://gitlab.com/vazhnov/linux_tty0_dynamic_info_screen)
* [Run software on the tty1 console instead of getty login on Ubuntu](https://raymii.org/s/tutorials/Run_software_on_tty1_console_instead_of_login_getty.html)

## Another info

## Copyright

Distributed under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) license or newer:

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://gitlab.com/vazhnov/linux_tty1_dynamic_info_screen">
    <span property="dct:title">Alexey Vazhnov</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">Dynamic info screen for Linux tty1 console</span>.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="RU" about="https://gitlab.com/vazhnov/linux_tty1_dynamic_info_screen">
  Russian Federation</span>.
</p>
